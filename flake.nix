{
  inputs = {
    hspkgs.url =
      "github:podenv/hspkgs/662e1c75a3b9a3873365b8cca041aaaa582e5c27";
    geomancy.url =
      "gitlab:dpwiz/geomancy/075cc36144c016970eb47623599a0e043e36fa3f";
    geomancy.flake = false;
    keid.url = "gitlab:keid/engine/5cecf08c672549a11a1c31674a97e2a9f948f3e8";
    keid.flake = false;
  };
  outputs = { self, hspkgs, geomancy, keid }:
    let
      pkgs = hspkgs.pkgs;
      gfx-pkgs = with pkgs; [
        vulkan-headers
        vulkan-tools
        vulkan-loader
        vulkan-validation-layers
        glslang

        xorg.xrandr
        xorg.libXcursor
        xorg.libXxf86vm
        xorg.libXinerama
        xorg.libXrandr
        xorg.libXi
        xorg.libXext
      ];
      add-gfx = drv: pkgs.haskell.lib.addPkgconfigDepends drv gfx-pkgs;

      haskellExtend = hpFinal: hpPrev:
        let
          mkKeidPkg = name:
            hpPrev.callCabal2nix "keid-${name}" "${keid}/${name}" { };
        in {
          keid-vkguide = hpPrev.callCabal2nix "keid-vkguide" self { };
          geomancy = hpPrev.callCabal2nix "geomancy" geomancy { };
          keid-core = mkKeidPkg "core";
          keid-render-basic = add-gfx (mkKeidPkg "render-basic");
          keid-geometry = mkKeidPkg "geometry";
          keid-ui-dearimgui = mkKeidPkg "ui-dearimgui";
          keid-resource-gltf = mkKeidPkg "resource-gltf";
          gltf-codec = pkgs.haskell.lib.doJailbreak (pkgs.haskell.lib.dontCheck
            (pkgs.haskell.lib.overrideCabal hpPrev.gltf-codec {
              broken = false;
            }));
          vulkan-utils = pkgs.haskell.lib.dontCheck hpPrev.vulkan-utils;
          bindings-GLFW = pkgs.haskell.lib.dontCheck
            (pkgs.haskell.lib.addPkgconfigDepends
              (pkgs.haskell.lib.disableCabalFlag
                (pkgs.haskell.lib.enableCabalFlag hpPrev.bindings-GLFW
                  "system-GLFW")
                "X") [ pkgs.glfw-wayland pkgs.wayland pkgs.libffi ]);
          dear-imgui = let
            pkg = pkgs.haskell.lib.disableCabalFlag
              (pkgs.haskell.lib.disableCabalFlag hpPrev.dear-imgui "opengl3")
              "sdl";
          in pkgs.haskell.lib.overrideCabal (pkgs.haskell.lib.doJailbreak
            (pkgs.haskell.lib.addPkgconfigDepends
              (pkgs.haskell.lib.enableCabalFlag
                (pkgs.haskell.lib.enableCabalFlag pkg "vulkan")
                "glfw") [ hpPrev.vulkan hpPrev.GLFW-b ])) {
                  broken = false;
                  libraryPkgconfigDepends = [
                    # pkgs.vulkan-headers
                    # pkgs.vulkan-tools
                    # pkgs.vulkan-loader
                    # pkgs.vulkan-validation-layers
                    pkgs.glfw-wayland
                    pkgs.wayland
                    pkgs.libffi
                    pkgs.pkg-config
                  ];
                  src = pkgs.fetchFromGitHub {
                    owner = "haskell-game";
                    repo = "dear-imgui.hs";
                    rev = "b48ef7904b10fe467b07088c452b6a64c1791409";
                    sha256 =
                      "sha256-V0mtzuJW/mbHe7gQlpuKaKP/NdZDKmVef0GXKVerwxo=";
                    fetchSubmodules = true;
                  };
                };
        };
      hsPkgs = pkgs.hspkgs.extend haskellExtend;

      # ghc = hsPkgs.ghcWithPackages (p: [ p.vulkan ]);
      ghc = hsPkgs.shellFor {
        packages = p: [ p.keid-vkguide ];
        buildInputs = with pkgs;
          [
            pkgs.nixVulkanIntel
            hpack
            cabal-install
            ghcid
            haskell-language-server
            pkg-config
            xorg.libXdmcp
          ] ++ gfx-pkgs;
      };

    in {
      packages.x86_64-linux.nixgl = pkgs.nixGL.nixVulkanIntel;
      haskellExtend = haskellExtend;
      devShell.x86_64-linux = ghc;
    };
}
