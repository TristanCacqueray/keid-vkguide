module OffscreenRender where

import RIO

import Engine.Stage.Component qualified as Stage
import Engine.StageSwitch (trySwitchStage)
import Engine.Types qualified as Engine
import Engine.Types qualified as Keid

-- rendering
import Engine.Vulkan.Swapchain (setDynamicFullscreen)
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (HasSwapchain, Queues, RenderPass (..))

import Render.Basic qualified as Basic
import Render.Samplers qualified as Samplers

import Render.Unlit.Textured.Model qualified as UnlitTextured

import Render.DescSets.Set0 qualified as Set0

-- resources
import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (withPools)
import Resource.Model qualified as Model
import Resource.Model.Observer (genericCreateInitial, genericDestroyCurrent, genericUpdateCoherent)
import Resource.Source qualified as Source

import Resource.Texture qualified as Texture
import Resource.Texture.Ktx2 qualified as Ktx2

import Control.Monad.Trans.Resource (ResourceT)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

-- helpers

import Geomancy

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Geomancy.Transform qualified as Transform
import Geomancy.Vulkan.View (lookAt)
import Geometry.Quad qualified as Quad
import RIO.State (gets)
import Render.Draw qualified as Draw

-- events

import Engine.Events qualified as Events
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key
import Geomancy.Vec3 qualified as Vec3
import Resource.Region qualified as Region

import Render.ColorTriangle.Pipeline qualified as Tri
import Render.Pass qualified as Pass
import Render.Pass.Offscreen (Offscreen)
import Render.Pass.Offscreen qualified as Offscreen

vec3p :: Float -> Float -> Float -> Vec3.Packed
vec3p x y z = Vec3.Packed (vec3 x y z)

type StageFrameRIO a = Keid.StageFrameRIO RenderPasses Pipelines FrameResources RunState a

stage :: Keid.Stage RenderPasses Pipelines FrameResources RunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering =
        Stage.Rendering
            { Stage.rAllocateRP = allocateRenderPass
            , Stage.rAllocateP = allocatePipelines
            }

    resources =
        Stage.Resources
            { Stage.rInitialRS = initialRunState
            , Stage.rInitialRR = intialRecyclableResources
            }

    scene =
        Stage.Scene
            { Stage.scBeforeLoop = void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            , Stage.scUpdateBuffers = updateBuffers
            , Stage.scRecordCommands = recordCommands
            }

    keyHandler (Events.Sink signal) _ (_, _, key) = case key of
        Key'Escape -> signal ()
        _ -> pure ()

    handleEvent () = void $ trySwitchStage Keid.Finish

data RenderPasses = RenderPasses
    { rpBasic :: Basic.RenderPasses
    , rpStatic :: Offscreen
    }
    deriving (Generic)

data Pipelines = Pipelines
    { pBasic :: Basic.Pipelines
    , pTri :: Tri.Pipeline
    }

instance RenderPass RenderPasses where
    updateRenderpass swapchain RenderPasses{..} =
        RenderPasses
            <$> updateRenderpass swapchain rpBasic
            <*> pure rpStatic

    refcountRenderpass RenderPasses{..} = do
        refcountRenderpass rpBasic
        refcountRenderpass rpStatic

-- See sky-playground Global.Render
allocateRenderPass ::
    HasSwapchain swapchain =>
    swapchain ->
    ResourceT (Keid.StageRIO st) RenderPasses
allocateRenderPass swapchain = do
    rpBasic <- Basic.allocate_ swapchain
    rpStatic <- Offscreen.allocate (settingsStatic swapchain)
    pure RenderPasses{..}

settingsStatic :: HasSwapchain swapchain => swapchain -> Offscreen.Settings
settingsStatic swapchain =
    Offscreen.Settings
        { sLabel = "Static"
        , sLayers = 1
        , sMultiView = False
        , sSamples = Vk.SAMPLE_COUNT_1_BIT
        , sExtent = Vk.Extent2D 800 600
        , sFormat = Vk.FORMAT_R8G8B8A8_UNORM
        , sDepthFormat = Swapchain.getDepthFormat swapchain
        , sColorLayout = Just Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        , sDepthLayout = Nothing
        , sMipMap = False
        }

allocatePipelines ::
    HasSwapchain swapchain =>
    swapchain ->
    RenderPasses ->
    ResourceT (Keid.StageRIO st) Pipelines
allocatePipelines swapchain rps = do
    samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
    Pipelines
        <$> Basic.allocatePipelines
            -- XXX: does [(), ()] define the texture layout?
            (Set0.mkBindings samplers [(), ()] Nothing 1)
            (Swapchain.getMultisample swapchain)
            rps.rpBasic
        <*> Tri.allocate ((settingsStatic swapchain).sSamples) rps.rpStatic

data RunState = RunState
    { rsSceneP :: Set0.Process
    , -- to draw texture
      rsQuadUV :: UnlitTextured.Model 'Buffer.Staged
    , rsQuadInstance :: UnlitTextured.Buffers
    , -- the texture
      rsTextures :: [Texture.Texture Texture.Flat]
    , -- trigger offscreen pass
      rsDraw :: Worker.Var Bool
    }

initialRunState :: Engine.StageRIO st (Resource.ReleaseKey, RunState)
initialRunState = Region.run do
    perspective <- Camera.spawnPerspective

    let staticView = lookAt (vec3 0 0 -1) (vec3 0 0 0) (vec3 0 -1 0)
    rsSceneP <- Worker.spawnMerge1 (mkScene staticView) perspective

    withPools \pools -> do
        logInfo "Loading texture now"
        rsTextures <- traverse (Ktx2.load pools)
            [ Source.File Nothing "assets/david.ktx2"
            ]
        logInfo "texture loaded!"

        rsQuadUV <- Model.createStagedL
            (Just "rsQuadUV")
            pools
            (Quad.toVertices Quad.texturedQuad)
            (Just Quad.indicesQuad)
        Model.registerIndexed_ rsQuadUV

        initial <- genericCreateInitial 3 "rsQuadInstance"
        rsQuadInstance <- genericUpdateCoherent initial $ UnlitTextured.attrStores
            [ UnlitTextured.attrs 0 0 [Transform.translate -1 -1 0]
            , UnlitTextured.attrs 0 0 [Transform.rotateY -1, Transform.rotateX -1]
            , UnlitTextured.attrs 1 1 [Transform.translate 1 1 0]
            ]
        context <- ask
        _key <- Resource.register $ genericDestroyCurrent context rsQuadInstance

        rsDraw <- Worker.newVar True

        pure RunState{..}

mkScene :: Transform -> Camera.Projection 'Camera.Perspective -> Set0.Scene
mkScene staticView Camera.Projection{..} =
    Set0.emptyScene
        { Set0.sceneProjection = projectionTransform
        , Set0.sceneView = staticView
        }

newtype FrameResources = FrameResources
    { frScene :: Set0.FrameResource '[Set0.Scene]
    }

intialRecyclableResources ::
    Queues Vk.CommandPool ->
    RenderPasses ->
    Pipelines ->
    ResourceT (Engine.StageRIO RunState) FrameResources
intialRecyclableResources _cmdPools rp pipelines = do
    textures <- gets rsTextures
    let extendedTextures = textures <> [Offscreen.colorTexture rp.rpStatic]
    frScene <- Set0.allocate (Basic.getSceneLayout pipelines.pBasic) extendedTextures [] Nothing mempty Nothing
    pure FrameResources{..}

updateBuffers :: RunState -> FrameResources -> StageFrameRIO ()
updateBuffers RunState{..} FrameResources{..} = do
    Set0.observe rsSceneP frScene

recordCommands :: Vk.CommandBuffer -> FrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb FrameResources{..} imageIndex = do
    quadsUV <- gets rsQuadUV
    quads <- gets rsQuadInstance

    Engine.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    let p = fPipelines.pBasic.pUnlitTexturedBlend

    -- See sky-playground Stage.Example.Scene.Sky
    vDraw <- gets rsDraw
    doDraw <- Worker.readVar vDraw
    when doDraw do
        Pass.usePass fRenderpass.rpStatic imageIndex cb do
            let viewport = Offscreen.oRenderArea fRenderpass.rpStatic
                scissor = Offscreen.oRenderArea fRenderpass.rpStatic
            Swapchain.setDynamic cb viewport scissor

            Set0.withBoundSet0 frScene p cb do
                Graphics.bind cb fPipelines.pTri do
                    Draw.triangle_ cb
        logInfo "done drawing..."
        Worker.pushInput vDraw (const False)

    Pass.usePass fRenderpass.rpBasic.rpForwardMsaa imageIndex cb do
        setDynamicFullscreen cb fSwapchainResources

        Set0.withBoundSet0 frScene p cb do
            Graphics.bind cb p do
                Draw.indexed cb quadsUV quads
