module TriangleColor where

-- The explanations assume that you start from the code of RedTriangle.
import RIO
import RIO.State (get)

import Engine.Stage.Component qualified as Stage
import Engine.Types qualified as Keid

import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
import Vulkan.Core10 qualified as Vk

import Render.ColorTriangle.Pipeline qualified as ColorTriangle
import Render.RedTriangle.Pipeline qualified as RedTriangle

import Engine.Vulkan.DescSets qualified as DescSets
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Render.Draw qualified as Draw

-- In this chapter, we'll setup a keyboard event handler and a RunState.

import Engine.Events qualified as Events
import Engine.StageSwitch (trySwitchStage)
import Engine.Window.Key (Key (..), KeyState (..))
import Engine.Window.Key qualified as Key
import Engine.Worker qualified as Worker
import Resource.Region qualified as Region
import UnliftIO.Resource qualified as Resource

-- The RunState
data PipelinePicker = Color | Red

type RunState = Worker.Var PipelinePicker

-- The events
data Events = ColorChanged | Quit

-- The pipelines
data Pipelines = Pipelines
    { redtrianglePipeline :: RedTriangle.Pipeline
    , colortrianglePipeline :: ColorTriangle.Pipeline
    }

-- Engine types
type StageFrameRIO a = Keid.StageFrameRIO Basic.RenderPasses Pipelines Stage.NoFrameResources RunState a

stage :: Keid.Stage Basic.RenderPasses Pipelines Stage.NoFrameResources RunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    -- Here we allocate the triangle pipelines
    rendering =
        Basic.rendering_
            { Stage.rAllocateP = \swapchain rps -> do
                Pipelines
                    <$> RedTriangle.allocate (Swapchain.getMultisample swapchain) rps.rpForwardMsaa
                    <*> ColorTriangle.allocate (Swapchain.getMultisample swapchain) rps.rpForwardMsaa
            }

    -- Here we allocate the RunState, which is a single pipeline picker var
    resources =
        Stage.Resources
            { rInitialRS = Resource.allocate (Worker.newVar Color) (const $ pure ())
            , rInitialRR = \_pool _rp _p -> pure Stage.NoFrameResources
            }

    scene =
        mempty
            { Stage.scRecordCommands = recordCommands
            , Stage.scBeforeLoop = do
                -- Here we setup a key handler
                void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
                logInfo "Press <space> to switch pipeline."
            }

    keyHandler (Events.Sink signal) _ (_, state, key) = case key of
        Key'Escape -> signal Quit
        Key'Space | state == KeyState'Pressed -> signal ColorChanged
        _ -> pure ()

    handleEvent = \case
        Quit -> void $ trySwitchStage Keid.Finish
        ColorChanged -> do
            -- Here we flip the pipeline picker var
            pipelinePickerVar <- get
            Worker.pushInput pipelinePickerVar $ \case
                Color -> Red
                Red -> Color

recordCommands :: Vk.CommandBuffer -> Stage.NoFrameResources -> Word32 -> StageFrameRIO ()
recordCommands cb _ imageIndex = do
    Keid.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    -- Here we use the pipeline picker var to pick the pipeline.
    pipelinePicker <- Worker.readVar =<< get
    let pipeline = case pipelinePicker of
            Color -> fPipelines.colortrianglePipeline
            Red -> fPipelines.redtrianglePipeline

    ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
        Swapchain.setDynamicFullscreen cb fSwapchainResources

        DescSets.withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS (pipeline.pLayout) mempty do
            Graphics.bind cb pipeline do
                Draw.triangle_ cb
