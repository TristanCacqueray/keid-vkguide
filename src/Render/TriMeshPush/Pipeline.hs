-- This pipeline definition just adds the push constant definition.
module Render.TriMeshPush.Pipeline (
    module Render.TriMesh.Pipeline,
    allocate,
) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (HasRenderPass (..), HasVulkan)
import Render.Code (compileFrag, compileVert)

import Render.TriMesh.Pipeline hiding (allocate)
import Render.TriMeshPush.Code qualified as Code

allocate ::
    ( HasVulkan env
    , HasRenderPass renderpass
    ) =>
    Vk.SampleCountFlagBits ->
    renderpass ->
    ResourceT (RIO env) Pipeline
allocate multisample rp = do
    snd <$> Graphics.allocate Nothing multisample configPush rp

configPush :: Config
configPush =
    Render.TriMesh.Pipeline.config
        { Graphics.cStages = Graphics.basicStages vertSpirv fragSpirv
        , Graphics.cPushConstantRanges =
            Vector.fromList
                [ Vk.PushConstantRange
                    { Vk.stageFlags = Vk.SHADER_STAGE_VERTEX_BIT
                    , Vk.offset = 0
                    , -- Here we define a new contant for the mat4 render_matrix
                      Vk.size = 64
                    }
                ]
        }

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
