-- |

module Render.ColorUV.Code (vert, frag) where

import RIO

import Render.Code (Code, glsl)

vert :: Code
vert =
    fromString
        [glsl|
    #version 450

    layout (location = 0) out vec2 outUV;

    void main() {
      // Normalize coordinate from [0..1]
      outUV = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);

      // Display full screen
      gl_Position = vec4(outUV * 2.0f - 1.0f, 0.0f, 1.0f);
    }
  |]

frag :: Code
frag =
    fromString
        [glsl|
    #version 450

    layout (location = 0) in vec2 inUV;
    layout (location = 0) out vec4 oColor;

    void main() {
      oColor = vec4(inUV, 0.0, 1.0);
    }
  |]
