module Render.TriMesh.Pipeline (
    Model,
    VertexAttrs (..),
    Pipeline,
    allocate,
    Config,
    config,
) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Vulkan.Core10 qualified as Vk

import Geomancy.Vec3 qualified as Vec3
import Resource.Model qualified as Model

import Engine.Vulkan.Format (HasVkFormat)
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Types (HasRenderPass (..), HasVulkan)
import Render.Code (compileFrag, compileVert)
import Render.TriMesh.Code qualified as Code

newtype VertexAttrs = VertexAttrs
    { vColor :: Vec3.Packed
    }
    deriving (Generic)
    deriving newtype (HasVkFormat, Storable)

-- Note: [Keid Model layout]
-- Model.Indexed create this buffer layout:
--   positions : attrs : instances
--
-- Here we store the position using Vec3.Packed
type Model buf = Model.Indexed buf Vec3.Packed VertexAttrs
type Vertex = Model.Vertex3d VertexAttrs

-- The pipeline would use 3d positions with some attributes, but no per-instance data
type Pipeline = Graphics.Pipeline '[] Vertex ()
type Config = Graphics.Configure Pipeline
type instance Graphics.Specialization Pipeline = ()

allocate ::
    ( HasVulkan env
    , HasRenderPass renderpass
    ) =>
    Vk.SampleCountFlagBits ->
    renderpass ->
    ResourceT (RIO env) Pipeline
allocate multisample rp =
    snd <$> Graphics.allocate Nothing multisample config rp

config :: Config
config =
    Graphics.baseConfig
        { Graphics.cStages = Graphics.basicStages vertSpirv fragSpirv
        , Graphics.cVertexInput = Graphics.vertexInput @Pipeline
        }

vertSpirv :: ByteString
vertSpirv = $(compileVert Code.vert)

fragSpirv :: ByteString
fragSpirv = $(compileFrag Code.frag)
