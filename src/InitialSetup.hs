module InitialSetup (stage) where

-- RIO is a standard library for writing production software.
import RIO

-- A stage is the basic unit of work.
import Engine.Types qualified as Keid (Stage)

-- A stage can be assemble using the Component api.
import Engine.Stage.Component qualified as Stage

-- Here we define a stage without any rendering, pipelines, resources or state.
stage :: Keid.Stage Stage.NoRenderPass Stage.NoPipelines Stage.NoFrameResources Stage.NoRunState
stage = Stage.assemble "Main" Stage.noRendering Stage.noResources Nothing
