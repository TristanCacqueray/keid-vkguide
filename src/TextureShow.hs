module TextureShow where

import RIO

import Engine.Stage.Component qualified as Stage
import Engine.StageSwitch (trySwitchStage)
import Engine.Types qualified as Engine
import Engine.Types qualified as Keid

-- rendering
import Engine.Vulkan.Swapchain (setDynamicFullscreen)
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Queues)

import Render.Basic qualified as Basic
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.Samplers qualified as Samplers

import Render.Unlit.Textured.Model qualified as UnlitTextured

import Render.DescSets.Set0 qualified as Set0

-- resources
import Engine.Camera qualified as Camera
import Engine.Worker qualified as Worker
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (withPools)
import Resource.Model qualified as Model
import Resource.Model.Observer (genericCreateInitial, genericDestroyCurrent, genericUpdateCoherent)

import Resource.Source qualified as Source
import Resource.Texture qualified as Texture
import Resource.Texture.Ktx2 qualified as Ktx2

import Control.Monad.Trans.Resource (ResourceT)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

-- helpers

import Geomancy

import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Geomancy.Transform qualified as Transform
import Geomancy.Vulkan.View (lookAt)
import RIO.State (gets)
import Render.Draw qualified as Draw

-- events

import Engine.Events qualified as Events
import Engine.Window.Key (Key (..))
import Engine.Window.Key qualified as Key
import Geomancy.Vec3 qualified as Vec3
import Resource.Region qualified as Region

vec3p :: Float -> Float -> Float -> Vec3.Packed
vec3p x y z = Vec3.Packed (vec3 x y z)

stage :: Basic.Stage FrameResources RunState
stage = Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering =
        Stage.Rendering
            { Stage.rAllocateRP = Basic.allocate_
            , Stage.rAllocateP = \swapchain rps -> do
                samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
                Basic.allocatePipelines
                    -- XXX: does [()] define the texture layout?
                    (Set0.mkBindings samplers [()] Nothing 1)
                    (Swapchain.getMultisample swapchain)
                    rps
            }

    resources =
        Stage.Resources
            { Stage.rInitialRS = initialRunState
            , Stage.rInitialRR = intialRecyclableResources
            }

    scene =
        Stage.Scene
            { Stage.scBeforeLoop = void $! Region.local $ Events.spawn handleEvent [Key.callback . keyHandler]
            , Stage.scUpdateBuffers = updateBuffers
            , Stage.scRecordCommands = recordCommands
            }

    keyHandler (Events.Sink signal) _ (_, _, key) = case key of
        Key'Escape -> signal ()
        _ -> pure ()

    handleEvent () = void $ trySwitchStage Keid.Finish

data RunState = RunState
    { rsSceneP :: Set0.Process
    , -- to draw texture
      rsQuadUV :: UnlitTextured.Model 'Buffer.Staged
    , rsQuadInstance :: UnlitTextured.Buffers
    , -- the texture
      rsTextures :: [Texture.Texture Texture.Flat]
    }

initialRunState :: Engine.StageRIO st (Resource.ReleaseKey, RunState)
initialRunState = Region.run do
    perspective <- Camera.spawnPerspective

    let staticView = lookAt (vec3 0 0 -1) (vec3 0 0 0) (vec3 0 -1 0)
    rsSceneP <- Worker.spawnMerge1 (mkScene staticView) perspective

    withPools \pools -> do
        logInfo "Loading texture now"
        rsTextures <- traverse (Ktx2.load pools)
            [ Source.File Nothing "assets/david.ktx2"
            ]
        logInfo "texture loaded!"

        let fullScreen =
                [ Model.Vertex (vec3p -1 -1 0) (vec2 0 0)
                , Model.Vertex (vec3p 1 -1 0) (vec2 1 0)
                , Model.Vertex (vec3p -1 1 0) (vec2 0 1)
                , Model.Vertex (vec3p -1 1 0) (vec2 0 1)
                , Model.Vertex (vec3p 1 -1 0) (vec2 1 0)
                , Model.Vertex (vec3p 1 1 0) (vec2 1 1)
                ]

        rsQuadUV <- Model.createStagedL (Just "quadUV") pools fullScreen Nothing
        Model.registerIndexed_ rsQuadUV

        initial <- genericCreateInitial 3 "rsQuadInstance"
        rsQuadInstance <- genericUpdateCoherent initial $ UnlitTextured.attrStores
            [ UnlitTextured.attrs 0 0 [Transform.translate -1 0 0]
            , UnlitTextured.attrs 0 0 [Transform.translate 1 0 0, Transform.rotateY 1]
            ]
        context <- ask
        _key <- Resource.register $ genericDestroyCurrent context rsQuadInstance

        pure RunState{..}

mkScene :: Transform -> Camera.Projection 'Camera.Perspective -> Set0.Scene
mkScene staticView Camera.Projection{..} =
    Set0.emptyScene
        { Set0.sceneProjection = projectionTransform
        , Set0.sceneView = staticView
        }

newtype FrameResources = FrameResources
    { frScene :: Set0.FrameResource '[Set0.Scene]
    }

intialRecyclableResources ::
    Queues Vk.CommandPool ->
    Basic.RenderPasses ->
    Basic.Pipelines ->
    ResourceT (Engine.StageRIO RunState) FrameResources
intialRecyclableResources _cmdPools _renderPasses pipelines = do
    textures <- gets rsTextures
    frScene <- Set0.allocate (Basic.getSceneLayout pipelines) textures [] Nothing mempty Nothing
    pure FrameResources{..}

updateBuffers ::
    RunState ->
    FrameResources ->
    Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
    Set0.observe rsSceneP frScene

recordCommands ::
    Vk.CommandBuffer ->
    FrameResources ->
    Word32 ->
    Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
    quadsUV <- gets rsQuadUV
    quads <- gets rsQuadInstance

    Engine.Frame{fSwapchainResources, fRenderpass, fPipelines} <- asks snd

    ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
        setDynamicFullscreen cb fSwapchainResources

        Set0.withBoundSet0 frScene (fPipelines.pUnlitColored) cb do
            Graphics.bind cb (fPipelines.pUnlitTexturedBlend) do
                Draw.indexed cb quadsUV quads
