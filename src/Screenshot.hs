{-# LANGUAGE OverloadedLists #-}

module Screenshot where

import RIO
import RIO.ByteString.Lazy qualified as BSL
import Vulkan.Zero (zero)

import Codec.Picture qualified as JP
import Codec.Picture.Types qualified as JP
import Control.Monad.Trans.Resource qualified as Resource
import Foreign (Storable (peek, sizeOf), plusPtr)
import Vulkan.CStruct.Extends (SomeStruct (..))
import Vulkan.Core10 qualified as Vk
import VulkanMemoryAllocator qualified as VMA

import Engine.Vulkan.Types qualified as Vulkan
import Resource.Image qualified as Image
import Resource.CommandBuffer qualified as CommandBuffer

outImageCI :: Vk.Format -> (Word32, Word32) -> Vk.ImageCreateInfo '[]
outImageCI fmt (width, height) =
    zero
        { Vk.imageType = Vk.IMAGE_TYPE_2D
        , Vk.flags = zero
        , Vk.format = fmt
        , Vk.extent = Vk.Extent3D width height 1
        , Vk.mipLevels = 1
        , Vk.arrayLayers = 1
        , Vk.tiling = Vk.IMAGE_TILING_LINEAR
        , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
        , Vk.usage = Vk.IMAGE_USAGE_TRANSFER_DST_BIT
        , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
        , Vk.samples = Vk.SAMPLE_COUNT_1_BIT
        }

outImageAllocationCI :: VMA.AllocationCreateInfo
outImageAllocationCI =
    zero
        { VMA.flags = VMA.ALLOCATION_CREATE_MAPPED_BIT
        , VMA.usage = VMA.MEMORY_USAGE_GPU_TO_CPU
        }

data ScreenshotImage = ScreenshotImage
    { info :: VMA.AllocationInfo
    , image :: Vk.Image
    , layout :: Vk.SubresourceLayout
    , dim :: (Word32, Word32)
    }

createScreenshotImage ::
    (HasLogFunc a, MonadReader a m, Vulkan.HasVulkan a, Resource.MonadResource m) =>
    Vk.Format ->
    (Word32, Word32) ->
    m (Resource.ReleaseKey, ScreenshotImage)
createScreenshotImage fmt dim = do
    context <- ask
    (outImage, outAllocation, outInfo) <-
        VMA.createImage
            (Vulkan.getAllocator context)
            (outImageCI fmt dim)
            outImageAllocationCI
    outKey <-
        Resource.register $
            VMA.destroyImage
                (Vulkan.getAllocator context)
                outImage
                outAllocation
    outLayout <-
        Vk.getImageSubresourceLayout
            (Vulkan.getDevice context)
            outImage
            ( Vk.ImageSubresource
                { aspectMask = Vk.IMAGE_ASPECT_COLOR_BIT
                , mipLevel = 0
                , arrayLayer = 0
                }
            )

    logInfo $ "Output layout: " <> displayShow outLayout
    pure (outKey, ScreenshotImage outInfo outImage outLayout dim)

takeScreenshot :: MonadIO m => Vk.CommandBuffer -> ScreenshotImage -> (Maybe Vk.ImageLayout, Vk.Image) -> m ()
takeScreenshot cb si (srcLayout, srcImage) = do
    -- see: https://github.com/SaschaWillems/Vulkan/blob/ed406e61a69a9fb5616e087c99291eb27ba2b9a9/examples/screenshot/screenshot.cpp#L239
    Vk.cmdPipelineBarrier
        cb
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        zero
        mempty
        mempty
        [ SomeStruct -- Transition dst image to transfer destination layout
            zero
                { Vk.srcAccessMask = zero
                , Vk.dstAccessMask = Vk.ACCESS_TRANSFER_WRITE_BIT
                , Vk.oldLayout = Vk.IMAGE_LAYOUT_UNDEFINED
                , Vk.newLayout = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
                , Vk.image = si.image
                , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
                }
        ]

    let sourceTransition =
            case srcLayout of
                Nothing ->
                    -- Unknown layout
                    Nothing
                Just Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL ->
                    -- Already there
                    Nothing
                _others ->
                    -- Do a transition, then restore the original
                    srcLayout
    for_ sourceTransition \oldLayout ->
        Vk.cmdPipelineBarrier
            cb
            Vk.PIPELINE_STAGE_TRANSFER_BIT
            Vk.PIPELINE_STAGE_TRANSFER_BIT
            zero
            mempty
            mempty
            [ SomeStruct -- Transition source image to from texture to transfer source.
                zero
                    { Vk.srcAccessMask = Vk.ACCESS_MEMORY_READ_BIT
                    , Vk.dstAccessMask = Vk.ACCESS_TRANSFER_READ_BIT
                    , Vk.oldLayout = oldLayout
                    , Vk.newLayout = Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
                    , Vk.image = srcImage
                    , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
                    }
            ]

    let imageSubr =
            Vk.ImageSubresourceLayers
                { aspectMask = Vk.IMAGE_ASPECT_COLOR_BIT
                , mipLevel = 0
                , baseArrayLayer = 0
                , layerCount = 1
                }

    Vk.cmdCopyImage
        cb
        srcImage
        Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        si.image
        Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        [ Vk.ImageCopy
            { srcSubresource = imageSubr
            , srcOffset = Vk.Offset3D 0 0 0
            , dstSubresource = imageSubr
            , dstOffset = Vk.Offset3D 0 0 0
            , extent = uncurry Vk.Extent3D si.dim 1
            }
        ]

    Vk.cmdPipelineBarrier
        cb
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        Vk.PIPELINE_STAGE_TRANSFER_BIT
        zero
        mempty
        mempty
        [ SomeStruct -- Transition destination image to general layout
            zero
                { Vk.srcAccessMask = Vk.ACCESS_TRANSFER_WRITE_BIT
                , Vk.dstAccessMask = Vk.ACCESS_MEMORY_READ_BIT
                , Vk.oldLayout = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
                , Vk.newLayout = Vk.IMAGE_LAYOUT_GENERAL
                , Vk.image = si.image
                , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
                }
        ]

    for_ sourceTransition \restoreLayout ->
        Vk.cmdPipelineBarrier
            cb
            Vk.PIPELINE_STAGE_TRANSFER_BIT
            Vk.PIPELINE_STAGE_TRANSFER_BIT
            zero
            mempty
            mempty
            [ SomeStruct -- Transition source image to from texture to transfer source.
                zero
                    { Vk.srcAccessMask = Vk.ACCESS_TRANSFER_WRITE_BIT
                    , Vk.dstAccessMask = Vk.ACCESS_MEMORY_READ_BIT
                    , Vk.oldLayout = Vk.IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
                    , Vk.newLayout = restoreLayout
                    , Vk.image = srcImage
                    , Vk.subresourceRange = Image.subresource Vk.IMAGE_ASPECT_COLOR_BIT 1 1
                    }
            ]

recordScreenshotCommands ::
  (MonadReader env m, Resource.MonadResource m, MonadUnliftIO m, HasLogFunc env, Vulkan.HasVulkan env) =>
  Vulkan.Queues Vk.CommandPool ->
  FilePath ->
  (Maybe Vk.ImageLayout, Vk.Image) ->
  m ()
recordScreenshotCommands queues fp src = do
    logDebug "Taking a screenshot"
    (siKey, si) <- Screenshot.createScreenshotImage Vk.FORMAT_R8G8B8A8_UNORM (800, 600)

    context <- ask
    CommandBuffer.oneshot_ context queues Vulkan.qTransfer \cb -> do
        Screenshot.takeScreenshot cb si src
    liftIO do
        Screenshot.writeImage fp =<< Screenshot.convertImage si
    Resource.release siKey


convertImage :: MonadIO m => ScreenshotImage -> m (JP.Image JP.PixelRGBA8)
convertImage si = do
    let -- pixelAddr :: Int -> Int -> Ptr Word32
        pixelAddr x y =
            plusPtr
                (VMA.mappedData si.info)
                ( fromIntegral si.layout.offset
                    + (y * fromIntegral si.layout.rowPitch)
                    + (x * sizeOf (0 :: Word32))
                )

    liftIO do
        JP.withImage
            (fromIntegral $ fst si.dim)
            (fromIntegral $ snd si.dim)
            ( \x y ->
                fmap
                    (JP.unpackPixel @JP.PixelRGBA8)
                    (peek $ pixelAddr x y)
            )

writeImage :: MonadIO m => FilePath -> JP.Image JP.PixelRGBA8 -> m ()
writeImage fp = BSL.writeFile fp . JP.encodePng
